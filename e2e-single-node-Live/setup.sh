#!/bin/bash

echo -e "\n=========================================================="
echo -e "-------------------- INSTALLING DOCKER ......"
echo -e "==========================================================\n\n"
# install latest version of docker the lazy way
curl -sSL https://get.docker.com | sh

# make it so you don't need to sudo to run docker commands
sudo groupadd docker
sudo usermod -aG docker root
#newgrp docker

# Docker Engine - Uninstall old versions
apt-get purge docker-ce docker-ce-cli containerd.io
apt-get remove docker docker-engine docker.io containerd runc
rm -rf /var/lib/docker
rm -rf /var/lib/containerd

echo -e "\n=========================================================="
echo -e "---------------- INSTALLING DOCKER ENGINE ......"
echo -e "==========================================================\n\n"

# Docker Engine - Set up the repository
apt-get update
apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


# Docker Engine - Install Docker Engine
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
echo -e "\n\n\n\n"

echo -e "\n=========================================================="
echo -e "---------------- INSTALLING DOCKER COMPOSE ......"
echo -e "==========================================================\n\n"
# Docker-Compose - install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
echo -e "\n\n\n\n"

# copy the dockerfile into /srv/docker 
# if you change this, change the systemd service file to match
# WorkingDirectory=[whatever you have below]
echo -e "\n=========================================================="
echo -e "---------------- Setting Up Tridash Environment ......"
echo -e "==========================================================\n\n"
mkdir -p /srv/td/live/docker
mkdir -p /srv/td/live/certificate
mkdir -p /srv/td/live/nginx/nginx-conf
mkdir -p /srv/td/live/nginx/nginx-conf-certificate
mkdir -p /srv/td/live/dhparam
mkdir -p /srv/td/live/dgraph/data

chmod -R 777 /srv/td

echo -e "\n---------------- Downloading - config files ----------------"
#curl -o /srv/td/live/docker/docker-compose.yml https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/docker/docker-compose-test.yml
echo -e "\n1. Downloading - docker-compose.yml ----------------\n"
curl -Lo /srv/td/live/docker/docker-compose.yml https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-Live/docker/docker-compose.yml
echo -e "\n\n2. Downloading - nginx.conf ----------------\n"
curl -Lo /srv/td/live/nginx/nginx-conf/nginx.conf https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-Live/nginx/nginx.conf
curl -Lo /srv/td/live/nginx/nginx-conf-certificate/nginx.conf https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-Live/nginx/nginx-certificate.conf
echo -e "\n\n3. Downloading - dhparam ----------------\n"
curl -Lo /srv/td/live/dhparam/dhparam-2048.pem https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-Live/dhparam/dhparam-2048.pem
echo -e "\n\n4. Downloading - certificate_renew.sh ----------------\n"
curl -Lo /srv/td/live/certificate/certificate_renew.sh https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-Live/certificate/certificate_renew.sh

# copy in systemd unit file and register it so our compose file runs 
# on system restart
echo -e "\n\n5. Downloading - docker-compose-app.service ----------------\n"
curl -Lo /etc/systemd/system/docker-compose-app.service https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-Live/docker/docker-compose-app.service

#systemctl enable docker-compose-app

docker login -u tdimages -p ekayaa2019

echo -e "\n==================***** SYSTEM ******==============="
echo -e "==================****** READY ******==============="
echo -e "==================******* FOR *******=============="
echo -e "==================****** REBOOT *****===============\n"
echo -e "!!! IMPORTANT : Please reboot the system now. !!!\n\n"
# start up the application via docker-compose
# docker-compose -f /srv/td/live/docker/docker-compose.yml up -d