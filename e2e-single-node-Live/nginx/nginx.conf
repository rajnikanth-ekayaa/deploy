proxy_ssl_session_reuse on;

# Cache for static assets
proxy_cache_path /tmp/nginx-cache levels=1:2 keys_zone=static-cache:10m max_size=128m inactive=7d use_temp_path=off;
proxy_cache_key $scheme$proxy_host$request_uri;
proxy_cache_lock on;
proxy_cache_use_stale updating;

# Cache for internal auth checks
proxy_cache_path /tmp/nginx-cache-auth levels=1:2 keys_zone=auth_cache:10m max_size=128m inactive=30m use_temp_path=off;

server {
        listen 80;
        listen [::]:80;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;

        server_name www.lifestyle.tridasha.in www.lifestyleshop.tridasha.in;

        location ~ /.well-known/acme-challenge {
                allow all;
                root /var/www/html;
        }

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location / {
                rewrite ^ https://$host$request_uri? permanent;
        }
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name www.lifestyle.tridasha.in www.lifestyleshop.tridasha.in;

        include /srv/nginx/nginx-conf/*.conf;

        server_tokens off;

        ssl_protocols TLSv1.2 TLSv1.3;

        ssl_early_data off;

        # turn on session caching to drastically improve performance
        ssl_session_cache builtin:1000 shared:SSL:5m;
        ssl_session_timeout 24h;
        
        # allow configuring ssl session tickets
        ssl_session_tickets off;
        
        # slightly reduce the time-to-first-byte
        ssl_buffer_size 8k;

        ssl_certificate /etc/letsencrypt/live/www.lifestyle.tridasha.in/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.lifestyle.tridasha.in/privkey.pem;

        ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

        ssl_prefer_server_ciphers on;

        ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;

        ssl_ecdh_curve auto;

        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8;

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location /assets/ {

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications
                
                proxy_cache static-cache;
                proxy_cache_valid 200 302 60m; ## cache successful responses for 60 minutes
                proxy_cache_valid 404 1m; ## expire 404 responses 1 minute
                proxy_cache_use_stale error timeout updating http_404 http_500 http_502 http_503 http_504;
                proxy_cache_bypass $http_x_purge $arg_nocache; ## To bypass cache https://www.indcatalog.in/?nocache=true
                add_header X-Cache-Status $upstream_cache_status;
                proxy_ignore_headers Cache-Control;
                proxy_hide_header Cache-Control;
                expires 60m; ## "Cache-Control: max-age=3600" tells client to cache for 60 minutes

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 6;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1000;
                gzip_types font/eot font/otf font/ttf font/woff2 font/woff image/svg+xml image/png text/css text/javascript application/javascript application/x-javascript;
                
                proxy_pass http://ui:8080;
        }

        location /api/ {

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 9;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1024;
                gzip_types application/json;

                proxy_pass http://indcatalog:3030;
        }

        location / {

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_set_header X-NginX-Proxy true;

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications

                proxy_pass http://ui:8080/;
        }

        root /var/www/html;
}

#Customer Specific Server Block - Bullseyebotanicals
server {
        listen 80;
        listen [::]:80;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;

        server_name www.bullseyebotanicals.in;

        location ~ /.well-known/acme-challenge {
                allow all;
                root /var/www/html;
        }

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location / {
                rewrite ^ https://$host$request_uri? permanent;
        }
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name www.bullseyebotanicals.in;

        server_tokens off;

        ssl_protocols TLSv1.2 TLSv1.3;

        ssl_early_data off;

        # turn on session caching to drastically improve performance
        ssl_session_cache builtin:1000 shared:SSL:5m;
        ssl_session_timeout 24h;
        
        # allow configuring ssl session tickets
        ssl_session_tickets off;
        
        # slightly reduce the time-to-first-byte
        ssl_buffer_size 8k;

        ssl_certificate /etc/letsencrypt/live/www.bullseyebotanicals.in/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.bullseyebotanicals.in/privkey.pem;

        ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

        ssl_prefer_server_ciphers on;

        ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;

        ssl_ecdh_curve auto;

        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8;

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location /assets/ {

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications
                
                proxy_cache static-cache;
                proxy_cache_valid 200 302 60m; ## cache successful responses for 60 minutes
                proxy_cache_valid 404 1m; ## expire 404 responses 1 minute
                proxy_cache_use_stale error timeout updating http_404 http_500 http_502 http_503 http_504;
                proxy_cache_bypass $http_x_purge $arg_nocache; ## To bypass cache https://www.indcatalog.in/?nocache=true
                add_header X-Cache-Status $upstream_cache_status;
                proxy_ignore_headers Cache-Control;
                proxy_hide_header Cache-Control;
                expires 60m; ## "Cache-Control: max-age=3600" tells client to cache for 60 minutes

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 6;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1000;
                gzip_types font/eot font/otf font/ttf font/woff2 font/woff image/svg+xml image/png text/css text/javascript application/javascript application/x-javascript;
                
                proxy_pass http://ui:8080;
        }

        location /api/ {

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 9;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1024;
                gzip_types application/json;

                proxy_pass http://indcatalog:3030;
        }

        location / {

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_set_header X-NginX-Proxy true;

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications

                proxy_pass http://ui:8080/;
        }

        root /var/www/html;
}

#Customer Specific Server Block - Toperas
server {
        listen 80;
        listen [::]:80;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;

        server_name www.toperas.com;

        location ~ /.well-known/acme-challenge {
                allow all;
                root /var/www/html;
        }

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location / {
                rewrite ^ https://$host$request_uri? permanent;
        }
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name www.toperas.com;

        server_tokens off;

        ssl_protocols TLSv1.2 TLSv1.3;

        ssl_early_data off;

        # turn on session caching to drastically improve performance
        ssl_session_cache builtin:1000 shared:SSL:5m;
        ssl_session_timeout 24h;
        
        # allow configuring ssl session tickets
        ssl_session_tickets off;
        
        # slightly reduce the time-to-first-byte
        ssl_buffer_size 8k;

        ssl_certificate /etc/letsencrypt/live/www.toperas.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.toperas.com/privkey.pem;

        ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

        ssl_prefer_server_ciphers on;

        ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;

        ssl_ecdh_curve auto;

        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8;

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location /assets/ {

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications
                
                proxy_cache static-cache;
                proxy_cache_valid 200 302 60m; ## cache successful responses for 60 minutes
                proxy_cache_valid 404 1m; ## expire 404 responses 1 minute
                proxy_cache_use_stale error timeout updating http_404 http_500 http_502 http_503 http_504;
                proxy_cache_bypass $http_x_purge $arg_nocache; ## To bypass cache https://www.indcatalog.in/?nocache=true
                add_header X-Cache-Status $upstream_cache_status;
                proxy_ignore_headers Cache-Control;
                proxy_hide_header Cache-Control;
                expires 60m; ## "Cache-Control: max-age=3600" tells client to cache for 60 minutes

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 6;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1000;
                gzip_types font/eot font/otf font/ttf font/woff2 font/woff image/svg+xml image/png text/css text/javascript application/javascript application/x-javascript;
                
                proxy_pass http://ui:8080;
        }

        location /api/ {

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 9;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1024;
                gzip_types application/json;

                proxy_pass http://indcatalog:3030;
        }

        location / {

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_set_header X-NginX-Proxy true;

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications

                proxy_pass http://ui:8080/;
        }

        root /var/www/html;
}

#Customer Specific Server Block - Hastha
server {
        listen 80;
        listen [::]:80;

        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;

        server_name www.hastha.in;

        location ~ /.well-known/acme-challenge {
                allow all;
                root /var/www/html;
        }

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location / {
                rewrite ^ https://$host$request_uri? permanent;
        }
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name www.hastha.in;

        server_tokens off;

        ssl_protocols TLSv1.2 TLSv1.3;

        ssl_early_data off;

        # turn on session caching to drastically improve performance
        ssl_session_cache builtin:1000 shared:SSL:5m;
        ssl_session_timeout 24h;
        
        # allow configuring ssl session tickets
        ssl_session_tickets off;
        
        # slightly reduce the time-to-first-byte
        ssl_buffer_size 8k;
        
        ssl_certificate /etc/letsencrypt/live/www.hastha.in/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.hastha.in/privkey.pem;

        ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

        ssl_prefer_server_ciphers on;

        ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;

        ssl_ecdh_curve auto;

        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8;

        location ~ (\.php$|\.jsp$|\.asp$|\.py$|\.perl$) {
                deny all;
                access_log off;
                log_not_found off;
        }

        location /assets/ {

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications
                
                proxy_cache static-cache;
                proxy_cache_valid 200 302 60m; ## cache successful responses for 60 minutes
                proxy_cache_valid 404 1m; ## expire 404 responses 1 minute
                proxy_cache_use_stale error timeout updating http_404 http_500 http_502 http_503 http_504;
                proxy_cache_bypass $http_x_purge $arg_nocache; ## To bypass cache https://www.indcatalog.in/?nocache=true
                add_header X-Cache-Status $upstream_cache_status;
                proxy_ignore_headers Cache-Control;
                proxy_hide_header Cache-Control;
                expires 60m; ## "Cache-Control: max-age=3600" tells client to cache for 60 minutes

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 6;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1000;
                gzip_types font/eot font/otf font/ttf font/woff2 font/woff image/svg+xml image/png text/css text/javascript application/javascript application/x-javascript;
                
                proxy_pass http://ui:8080;
        }

        location /api/ {

                gzip on;
                gzip_disable "msie6";
                gzip_vary on;
                gzip_proxied any;
                gzip_comp_level 9;
                gzip_buffers 16 8k;
                gzip_http_version 1.1;
                gzip_min_length 1024;
                gzip_types application/json;

                proxy_pass http://indcatalog:3030;
        }

        location / {

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_set_header X-NginX-Proxy true;

                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header X-XSS-Protection "1; mode=block" always;
                add_header X-Content-Type-Options "nosniff" always;
                add_header Referrer-Policy "no-referrer-when-downgrade" always;
                add_header Content-Security-Policy "default-src * data: 'unsafe-eval' 'unsafe-inline'" always;
                # add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
                # enable strict transport security only if you understand the implications

                proxy_pass http://ui:8080/;
        }

        root /var/www/html;
}