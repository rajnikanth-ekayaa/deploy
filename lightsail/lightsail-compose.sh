#!/bin/bash

# install latest version of docker the lazy way
curl -sSL https://get.docker.com | sh

# make it so you don't need to sudo to run docker commands
sudo usermod -aG docker ubuntu

# install docker-compose
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# copy the dockerfile into /srv/docker 
# if you change this, change the systemd service file to match
# WorkingDirectory=[whatever you have below]
mkdir -p /srv/td/docker
mkdir -p /srv/td/elastic/data
mkdir -p /srv/td/dgraph/data

#curl -o /srv/td/docker/docker-compose.yml https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/docker/docker-compose-test.yml
curl -o /srv/td/docker/docker-compose.yml https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/docker/docker-compose.yml
curl -o /srv/td/elastic/elasticsearch.yml https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/docker/elasticsearch.yml

# copy in systemd unit file and register it so our compose file runs 
# on system restart
curl -o /etc/systemd/system/docker-compose-app.service https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/docker/docker-compose-app.service
systemctl enable docker-compose-app

docker login -u tdimages -p ekayaa2019

# start up the application via docker-compose
docker-compose -f /srv/td/docker/docker-compose.yml up -d