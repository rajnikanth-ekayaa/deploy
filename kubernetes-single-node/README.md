    /****************** Workspace Setup ****************************************************************************************************************/
    
    a. Install kubectl
        - https://kubernetes.io/docs/tasks/tools/install-kubectl/
        - Navigate to your home directory:
            > mkdir .kube
        - Create a directory called configs within ~/.kube. You can use this directory to store your kubeconfig files.
            > mkdir configs
        - Download kubeconfig file from Linode Kubernetes Cluster
            - To access your cluster’s kubeconfig, log into your Cloud Manager account and navigate to the Kubernetes section.
            - From the Kubernetes listing page, click on your cluster’s more options ellipsis and select Download kubeconfig. The file will be saved to your computer’s Downloads folder.
        - Copy your K8-LKE-kubeconfig.yaml file to the ~/.kube/configs directory.
            > cp ./kubeconfig/K8-LKE-kubeconfig.yaml ~/.kube/configs/K8-LKE-kubeconfig.yaml
        - Open up your Bash profile (e.g. ~/.bash_profile) in the text editor of your choice and add your configuration file to the $KUBECONFIG PATH variable. If an export KUBECONFIG line is already present in the file, append to the end of this line as follows; if it is not present, add this line to the end of your file.
            > export KUBECONFIG=$KUBECONFIG:~/.kube/config:~/.kube/configs/kubeconfig.yaml
        - Close your terminal window and open a new window to receive the changes to the $KUBECONFIG variable.
            > kubectl version
            > kubectl config get-contexts (list all preconfigured contexts and see which one is active:)
            > kubectl config use-context <context-name> (Switch to a context/cluster > #NAME)
            > kubectl get pods -A
        
    b. Install helm
        - https://helm.sh/docs/intro/install/
        - Add a chart repository
            > helm repo add stable https://kubernetes-charts.storage.googleapis.com/
            > helm repo add stable https://charts.helm.sh/stable
            > helm repo update  
    

    /****************** Worker Node ********************************************************************************************************************/

    1. Setup Worker nodes in Linode cluster
        a. Teardown worker nodes (See below section - TearDown/Up > Worker Node Maintenance > a).
        b. Power Off the node.
        c. Goto Settings/Reset the root password. (password : parvati@shiva)
        d. Power On the node & Launch LISH console (login : root | password : parvati@shiva)
        e. Check/Create volume path on k8's node machine.
            > cd /
            > mkdir -p /tridasha/data/dgraph/
            > mkdir /tridasha/data/prometheus/
        f. To bring up the node (See below section - TearDown/Up > Worker Node Maintenance > b).

    /****************** Storage ************************************************************************************************************************/

    2. Set up storage
        a. Check volume path on k8's node machine.
            > cd /tridasha/data/dgraph/
            > cd /tridasha/data/prometheus/
        b. Create Storage Class on K8's cluster
            > kubectl get sc
            > kubectl create -f ./storage/local-storage.yaml
            > kubectl get sc
        c. Create Persistent Volumes for Dgraph & Prometheus(Monitoring)
            - Edit ./node/storage/persistent-volume-dgraph.yaml
                + add all k8's node name in the list.
            - Edit ./node/storage/persistent-volume-monitoring-alertmanager.yaml
                + add all k8's node name in the list.
            - Edit ./node/storage/persistent-volume-monitoring-prometheus.yaml
                + add all k8's node name in the list.
            > kubectl get pv
            > kubectl create -f ./storage/persistent-volume-dgraph.yaml
            > kubectl create -f ./storage/persistent-volume-monitoring-alertmanager.yaml
            > kubectl create -f ./storage/persistent-volume-monitoring-prometheus.yaml
            > kubectl get pv


    /****************** Dgraph *************************************************************************************************************************/

    3. Setup Dgraph Cluster
        a. Start Dgraph in Single instance mode
            > kubectl create -f ./pod/dgraph-single.yaml
            > kubectl get all -l app=dgraph -o=wide
            > kubectl get pvc
            > kubectl get pv
            > ls -all /data/dgraph/
            > kubectl logs --follow dgraph-0 alpha


    /****************** App Server *********************************************************************************************************************/

    4. Setup App Server Cluster
        a. Configure docker hub registry with credentials
            > kubectl get secret
            - Run the commands in ./pod/secret.txt
            > kubectl get secret
        b. Start App Server in HA mode.
            > kubectl create -f ./pod/app-server-HA.yaml 
            > kubectl get all -l app=indcatalog
            > kubectl logs --tail=100 -l app=indcatalog
        c. Remove App Server in HA mode.
            > kubectl delete -f ./pod/app-server-HA.yaml 
            > kubectl get pods

    /****************** UI Node Server *********************************************************************************************************************/

    4. Setup App Server Cluster
        a. Configure docker hub registry with credentials
            > kubectl get secret
            - Run the commands in ./pod/secret.txt
            > kubectl get secret
        b. Start UI Server in HA mode.
            > kubectl create -f ./pod/ui-server-HA.yaml 
            > kubectl get all -l app=ui
            > kubectl logs --tail=100 -l app=ui
        c. Remove UI Server in HA mode.
            > kubectl delete -f ./pod/ui-server-HA.yaml 
            > kubectl get pods


    /****************** Prometheus (Monitoring) *******************************************************************************************************/

    5. Setup Prometheus (Monitoring) Server
        a. Install helm tool : https://helm.sh/docs/intro/install/
        b. Add stable repo to helm.
            > helm repo add stable https://kubernetes-charts.storage.googleapis.com
        c. Start Prometheus setup.
            - Create 'Monitoring' namespace for prometheus setup
                > kubectl get ns
                > kubectl create namespace monitoring
                > kubectl get ns
            > helm ls --namespace monitoring
            > helm install monitoring --namespace monitoring stable/prometheus-operator --values ./monitoring/prometheus-operator/prometheus.values.yaml
            > kubectl get all --namespace monitoring
            > helm ls --namespace monitoring
            > kubectl get pvc --namespace monitoring
        d. Import Grafana dashboard 
            - import 'grafana_dashboard.json' in grafana ui from ./monitoring/grafana-dgraph.
            - import all JSON files in grafana ui from ./monitoring/grafana-nginx.


    /****************** Kubernetes Web UI (Dashboard) *******************************************************************************************************/

    5. Setup Prometheus (Monitoring) Server
        a. Add stable repo to helm.
            > helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
        b. Start Kubernetes Web UI setup.
            > helm ls
            > helm install webui --namespace monitoring kubernetes-dashboard/kubernetes-dashboard --set nodeSelector.nodeFor=tools,service.externalPort=7000
            > helm ls
        c. To access dashboard
            > kubectl proxy
            - Got to browser and type below URL
                > http://localhost:8001/api/v1/namespaces/monitoring/services/https:webui-kubernetes-dashboard:7000/proxy/
                - Choose KubeConfig File Option
                - choose file from ./kubeconfig/K8-LKE-kubeconfig.yaml


    /****************** TLS Certificate ****************************************************************************************************************/

    7. Install Certificate for HTTPS
        a. Install cert-manager
            > kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.1/cert-manager.crds.yaml
            > kubectl create namespace cert-manager
            > helm repo add jetstack https://charts.jetstack.io
            > helm repo update
            > helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.15.1
            > kubectl create -f ./certificate/acme-issuer-prod.yaml
        b. Debug Commands
            > kubectl get CertificateRequest -o wide
            > kubectl get certificates -o wide


    /****************** Ingress ************************************************************************************************************************/

    6. Setup Ingress Controller (to handle request redirection based on incoming host/domain name)
        a. Install nginx-ingress controller on type 'LoadBalancer'
            > helm install nginx-ingress stable/nginx-ingress --values ./ingress/nginx.values.yaml
                OR
            > helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
            > helm repo update
            > helm install ingress-nginx ingress-nginx/ingress-nginx --version 3.26.0 --values ./ingress/nginx.values.yaml 
            > kubectl get svc -A -o=wide
            - Update the LoadBalancer External IP in GoDaddy.
        b. Configure ingress resources for services to be access over internet
            -  Create ingress resource for Indcatalog service
                > kubectl get ingress -A
                > kubectl create -f ./ingress/nginx-ingress-indcatalog.yaml
                > kubectl get ingress -A
            -  Create ingress resource for Monitoring service
                > kubectl get ingress -A
                > kubectl create -f ./ingress/nginx-ingress-monitoring.yaml
                > kubectl get ingress -A
        c. Update HELM charts / nginx-ingress controller
            - push nginx config changes to existing nginx-ingress controller 
                > helm upgrade --values ./ingress/nginx.values.yaml nginx-ingress stable/nginx-ingress 
                    OR
                > helm upgrade --values ./ingress/nginx.values.yaml ingress-nginx ingress-nginx/ingress-nginx --version 3.26.0

    /****************** Update App Server **************************************************************************************************************/

    7. Update Indcatalog Server
        a. Add the latest version-tag of the image (eg: app-v1.0.0.5)
            > kubectl get all -l app=indcatalog
            > kubectl set image deployments/indcatalog indcatalog=tdimages/tdrepo:<version-tag>
            > kubectl set image deployments/ui ui=tdimages/tdrepo:<version-tag>
        b. Rollback to previous Update
            - List all the previous deployment 
                > kubectl rollout history <deployment-name> (eg:- deployment.apps/indcatalog)
            - Check the version in the above list to rollback to
                > kubectl rollout history <deployment-name> --revision=<revision-number>
            - Execute Rollback to given revision number
                > kubectl rollout undo <deployment-name> --to-revision=<revision-number>


====== TearDown/Up =================================================================================================================================================


1. Delete Prometheus (Monitoring) Server
    a. Remove Prometheus (Monitoring) installation
        > helm delete monitoring --namespace monitoring
    b. Delete all the Persistent Volume Claims (PVC)
        > kubectl delete pvc --namespace monitoring --all
    c. Flush/Patch Persistent Volumes related to Prometheus (Monitoring)
        > kubectl patch pv monitoring-alertmanager-pv  -p '{"spec":{"claimRef": null}}'
        > kubectl patch pv monitoring-prometheus-pv  -p '{"spec":{"claimRef": null}}'


2. Worker Node Maintenance
    a. To bring a worker node DOWN gracefully  
        > kubectl drain <node-name>  --ignore-daemonsets --delete-local-data
        - Execute the maintenance activiy
    b. To bring a worker node UP gracefully
        > kubectl uncordon <node-name>
    c. Above process take's some time to bring the node to 'Ready' state.


===== Kubernetes Command ========================================================================================================================================

1. Get all running pods in a given worker node
    > kubectl get pods --all-namespaces -o wide --field-selector spec.nodeName=<node-name>
2. Get All resources for Indcatalog(AppServer)
    > kubectl get all -l app=indcatalog (fetch all resources for App Server)
3. Get Logs
    > kubectl logs -f <pod-name>
    > kubectl logs -f -l component=speaker
4. Delete a pod forcefully
    > kubectl delete pod <pod-name> --grace-period=0 --force --namespace <namespace-name>
5. Delete all pods in a namespace
    > kubectl delete --all pods --namespace=<namespace-name>


===== Dgraph DATA Backup ========================================================================================================================================

1. Connect to 'server' container inside 'dgraph' pod 
    - kubectl exec -it <pod-name> --container <container-name-inside-pod> -- /bin/bash
        > kubectl exec -it dgraph-0 --container server -- /bin/bash
2. Then fire curl command inside the container 
    > curl http://localhost:8080/admin/export
3. The above command will create an 'export' folder in '/dgraph' directory in the container.
4. The 'export' folder will have two files generated
    - dgraph-1-2021-03-28-13-01.rdf.gz
    - dgraph-1-2021-03-28-13-01.schema.gz
4. The directory '/dgraph' of container is mapped to the node directory '/data/dgraph'. 
5. The same files are now accessible using ssh to the node -> cd /data/dgraph
6. To tranfer the data from remote server to local machine
    - scp -r <username>@<machine-IP>:<remote-machine-path> <local-machine-path>
        > scp -r  root@172.105.48.191:/data/dgraph/export /Download/dgraph_data/

===== Dgraph DATA RESTORE/IMPORT ===============================================================================================================================

1. To tranfer the data from local machine to remote server
    - scp -r <local-machine-path> <username>@<machine-IP>:<remote-machine-path>
        > scp -r  /Upload/dgraph_data/ root@172.105.48.191:/data/import-data

2. Connect to 'server' container inside 'dgraph' pod 
    - kubectl exec -it <pod-name> --container <container-name-inside-pod> -- /bin/bash
        > kubectl exec -it dgraph-0 --container alpha -- /bin/bash
    - fire the data import command, dgraph live -r <path-to-rdf-gz> -s <path-to-schema-gz>
        > dgraph live --files /dgraph/import/live/g01.rdf.gz --schema /dgraph/import/live/g01.schema.gz --zero zero:5080
        OR
        > dgraph live --files /dgraph/import/live/g01.rdf.gz --schema /dgraph/import/live/g01.schema.gz --zero dgraph-0.dgraph.default.svc.cluster.local:5080
