#!/bin/bash
cd "$(dirname "$0")"

appDir=../build-playground/app

rm -rf $appDir

mkdir "$appDir"

#git clone --single-branch --branch release/Core-4.5.3 https://rajnikanth-ekayaa@bitbucket.org/maniniek/tridasha.git $appDir/
git clone https://rajnikanth-ekayaa@bitbucket.org/maniniek/tridasha.git $appDir/

cd $appDir

mvn clean compile assembly:single

OUTPUT=$(ls -l -d ./IndCatalog/target/)
echo "${OUTPUT}"