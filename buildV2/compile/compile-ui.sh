#!/bin/bash
cd "$(dirname "$0")"

guiDir=../build-playground/ui

rm -rf $guiDir

mkdir "$guiDir"

git clone https://rajnikanth-ekayaa@bitbucket.org/rajnikanth-ekayaa/new-ui.git $guiDir/
#git clone --single-branch --branch feature/Kustomization https://rajnikanth-ekayaa@bitbucket.org/rajnikanth-ekayaa/new-ui.git $guiDir/
#git clone --single-branch --branch feature/SEO https://rajnikanth-ekayaa@bitbucket.org/rajnikanth-ekayaa/new-ui.git $guiDir/

cd $guiDir

npm install --legacy-peer-deps

cp -r ./libs/ ./node_modules/

npm run prod-build