#!/bin/bash
cd "$(dirname "$0")"

versionFile='version.properties'

#Read the previous version and update
if [ -f "$versionFile" ]
then
  while IFS='=' read -r key value
  do
    key=$(echo $key)
    eval ${key}=\${value}
  done < "$versionFile"

  echo "Current Version = " ${version_ui_test}
  
  perl -pe '/^version_ui_test=/ and s/(\d+\.\d+\.\d+\.)(\d+)/$1 . ($2+1)/e' -i $versionFile
else
  echo "$versionFile not found."
fi

#Read the updated version and start building.
if [ -f "$versionFile" ]
then
    while IFS='=' read -r key value
    do
    key=$(echo $key)
    eval ${key}=\${value}
    done < "$versionFile"

    echo "New Version = " ${version_ui_test}

    imageName="tdimages/tdrepo:ui-test-v${version_ui_test}"

    docker build -f ./docker/ui/Dockerfile-Test -t ${imageName} ../build-playground/

    docker login -u tdimages -p ekayaa2019

    docker push ${imageName}

else
  echo "$versionFile not found."
fi

