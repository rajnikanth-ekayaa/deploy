#!/bin/bash
cd "$(dirname "$0")"


# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

all(){
	echo "Building All"
    ./compile/compile-all.sh
    ./package/package-app.sh
	./package/package-ui.sh
}

allAuto(){
	echo "Building All (Auto)"
    ./compile/compile-all-auto.sh
    ./package/package-app-auto.sh
	./package/package-ui-auto.sh
}

allTest(){
	echo "Building All (Test)"
    ./compile/compile-all-test.sh
    ./package/package-app-test.sh
	./package/package-ui-test.sh
}
 
ui(){
	echo "Building UI"
    ./compile/compile-ui.sh
    ./package/package-ui.sh
}

uiAuto(){
	echo "Building UI (Auto)"
    ./compile/compile-ui-auto.sh
    ./package/package-ui-auto.sh
}

uiTest(){
	echo "Building UI (Test)"
    ./compile/compile-ui-test.sh
    ./package/package-ui-test.sh
}

app(){
	echo "Building APP"
    ./compile/compile-app.sh
    ./package/package-app.sh
}

appAuto(){
	echo "Building APP (Auto)"
    ./compile/compile-app-auto.sh
    ./package/package-app-auto.sh
}

appTest(){
	echo "Building APP (Test)"
    ./compile/compile-app-test.sh
    ./package/package-app-test.sh
}

# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. All"
	echo "2. All (Auto)"
	echo "3. All (Test)"
	echo "4. UI"
	echo "5. APP"
	echo "6. UI (Auto)"
	echo "7. APP (Auto)"
	echo "8. UI (Test)"
	echo "9. APP (Test)"
	echo "10. Exit"
}

# read input from the keyboard and take a action
# invoke the All() when the user select 1 from the menu option.
# invoke the ui() when the user select 3 from the menu option.
# invoke the app() when the user select 4 from the menu option.
# Exit when user the user select 7 from the menu option.
read_options(){
	local choice
	read -p "Enter choice [ 1 - 10 ] " choice
	case $choice in
		1) all ;;
		2) allAuto ;;
		3) allTest ;;
		4) ui ;;
		5) app ;;
		6) uiAuto ;;
		7) appAuto ;;
		8) uiTest ;;
		9) appTest ;;
		10) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap 'exit' SIGINT SIGQUIT SIGTSTP

# -----------------------------------
# Step #4: Main logic - infinite loop
# ------------------------------------
while true
do 
	show_menus
	read_options
done

