#!/bin/bash
cd "$(dirname "$0")"


# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

all(){
	echo "Building All"
    ./build-all.sh
}
 
ui(){
	echo "Building UI"
    ./build-ui.sh
}

app(){
	echo "Building APP"
    ./build-app.sh
}

# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. All"
	echo "2. UI"
	echo "3. APP"
	echo "4. Exit"
}

# read input from the keyboard and take a action
# invoke the All() when the user select 1 from the menu option.
# invoke the ui() when the user select 2 from the menu option.
# invoke the app() when the user select 3 from the menu option.
# Exit when user the user select 4 from the menu option.
read_options(){
	local choice
	read -p "Enter choice [ 1 - 4 ] " choice
	case $choice in
		1) all ;;
		2) ui ;;
		3) app ;;
		4) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap 'exit' SIGINT SIGQUIT SIGTSTP

# -----------------------------------
# Step #4: Main logic - infinite loop
# ------------------------------------
while true
do 
	show_menus
	read_options
done

