#!/bin/bash
cd "$(dirname "$0")"

appDir=./app

rm -rf $appDir
mkdir "$appDir"

git clone https://rajnikanth-ekayaa@bitbucket.org/maniniek/tridasha.git $appDir/

"$appDir/build/docker-build.sh"
"$appDir/build/docker-publish.sh"