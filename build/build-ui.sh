#!/bin/bash
cd "$(dirname "$0")"

guiDir=./ui

rm -rf $guiDir
mkdir "$guiDir"

git clone https://rajnikanth-ekayaa@bitbucket.org/rajnikanth-ekayaa/new-ui.git $guiDir/

"$guiDir/build/docker-build.sh"
"$guiDir/build/docker-publish.sh"
