/****************** Environment Setup **********************************************************************************************/

1. Update domain name in below script
    a. docker-compose.yml
        1. services > indcatalog > environment >  webServerName
        2. services > certbot > command
    b. nginx.conf
        1. server[80] > server_name
        2. server[433] > server_name
        3. server[433] > ssl_certificate, ssl_certificate_key
    c. nginx-certificate.conf
        1. server > server_name

2. Download the setup.sh from git repo.
    > cd /srv
    > curl -o setup.sh https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node/setup.sh
    > chmod +x ./setup.sh
    > ./setup.sh

3. Restart the E2E node. After restart check the docker service status.
    > systemctl status docker.service

4. Run docker compose to generate SSL Certificate for root domain.
    > docker-compose -f /srv/td/docker/docker-compose.yml up -d nginx-certificate
    > docker-compose -f /srv/td/docker/docker-compose.yml up -d certbot
    > docker-compose ps

5. Stop nginx-certificate for root domain.
    > docker-compose -f /srv/td/docker/docker-compose.yml down

4. Run docker compose to generate SSL Certificate for custom domain.
    - Pull the custom domain nginx-certificate.conf file.
        > curl -Lo /srv/td/nginx/nginx-conf-certificate/nginx.conf https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node/nginx/nginx-certificate-<custom-domain-name>conf

    > docker-compose -f /srv/td/docker/docker-compose.yml up -d nginx-certificate
    > docker-compose -f /srv/td/docker/docker-compose.yml up -d certbot
    > docker-compose ps

5. Stop nginx-certificate for custom domain.
    > docker-compose -f /srv/td/docker/docker-compose.yml down

6. Run docker compose to start the application.
    > docker-compose -f /srv/td/docker/docker-compose.yml up -d zero alpha
    > docker-compose -f /srv/td/docker/docker-compose.yml up -d

7. To auto renew ssl certificate
    > cd /srv/td/certificate
    > chmod +x certificate_renew.sh
    > sudo crontab -e
    > At the bottom of the file, add the following line: ` 0 12 * * * /srv/td/certificate/certificate_renew.sh >> /var/log/cron.log 2>&1 `

8. To auto start docker on system restart
    > systemctl enable docker-compose-app

9. References
    a. https://www.digitalocean.com/community/tutorials/how-to-secure-a-containerized-node-js-application-with-nginx-let-s-encrypt-and-docker-compose
    b. To bring down a specific service.
        > docker-compose rm -s -v <service-name>
    c. To stop the application.
        > docker-compose -f /srv/td/docker/docker-compose.yml down
    d. To update app with new release.
        > docker-compose up --force-recreate --build -d
        > docker image prune -f
    e. To check if the nginx conf is loaded with correct file/changes
        > docker exec -it nginx-certificate nginx -T