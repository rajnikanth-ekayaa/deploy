#!/bin/bash
echo -e "\n=========================================================="
echo -e "---------------- Setting Up Tridash DEV Environment ......"
echo -e "==========================================================\n\n"
mkdir -p /srv/td_dev/docker
mkdir -p /srv/td_dev/nginx/nginx-conf
mkdir -p /srv/td_dev/dgraph/data

chmod -R 777 /srv/td_dev

echo -e "\n---------------- Downloading - config files ----------------"

echo -e "\n1. Downloading - docker-compose-dev.yml ----------------\n"
curl -Lo /srv/td_dev/docker/docker-compose-dev.yml https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-dev/docker/docker-compose-dev.yml

echo -e "\n\n2. Downloading - nginx-dev.conf ----------------\n"
curl -Lo /srv/td_dev/nginx/nginx-conf/nginx-dev.conf https://bitbucket.org/rajnikanth-ekayaa/deploy/raw/master/e2e-single-node-dev/nginx/nginx-dev.conf

docker login -u tdimages -p ekayaa2019