#!/bin/bash
cd "$(dirname "$0")"

SERVER_FILES="./test-server/*"

for serverFile in $SERVER_FILES
do
    echo -e "\n================================"
    echo -e "\n Backup Started for $serverFile \n"
    echo -e "================================\n"

    #Read the previous version and update
    if [ -f "$serverFile" ]
    then
        while IFS='=' read -r key value
        do
            key=$(echo $key)
            eval ${key}=\${value}
        done < "$serverFile"

        BACKUP_DIR_PATH=${backupRootDir}$(date +%B_%d_%Y)${backupDir}
        mkdir -p "${BACKUP_DIR_PATH}"
        echo -e "\n1. Backup Directory Created : ${BACKUP_DIR_PATH}"

        echo -e "\n2. Connecting to Docker Server \n"

        echo -e "\n3. Running Database Export \n"
        
        ssh -t $serverIP "docker exec -it alpha curl http://localhost:8080/admin/export"

        sleep 1

        EXPORT_DIR=$(ssh -t $serverIP 'docker exec -it alpha bash -c "ls -t export | head -n 1"')

        echo $EXPORT_DIR

        echo -e "\n4. Downloading backup ...\n"

        sleep 1

        EXPORT_DIR_PATH=${exportRootDir}${EXPORT_DIR%%[[:cntrl:]]}

        echo -e "\n Remote Server IP > $serverIP"
        
        echo -e "\n Copying from > $EXPORT_DIR_PATH"

        echo -e "\n Copying to > $BACKUP_DIR_PATH"

        scp -r "$serverIP:$EXPORT_DIR_PATH" "$BACKUP_DIR_PATH"

    else
        echo "$serverFile not found."
    fi

    echo -e "\n================================"
    echo -e "\n Backup Completed \n"
    echo -e "================================\n"
    echo -e "\n**********************************************************************************************\n"
done
