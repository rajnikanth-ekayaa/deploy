/****************** E2E-PF9 Workspace Setup ****************************************************************************************************************/

1. Generate an SSH Key Pair on Linux And macOS (https://www.linode.com/docs/guides/use-public-key-authentication-with-ssh/)
    > ssh-keygen -b 4096 (Press Enter to use the default names id_rsa and id_rsa.pub in the /home/your_username/.ssh directory before entering your passphrase.)
    > Press Enter for passphrase

2. Remotely Connect to node using ssh
    > ssh root@X.X.X.X (eg: ssh root@164.52.210.195)

3. Restart the node once after creation

4. Install Platform9 on each node.
    > bash <(curl -sL https://pmkft-assets.s3-us-west-1.amazonaws.com/pf9ctl_setup)
    > pf9ctl config set
        > Platform9 Account URL: https://pmkft-1615937734-92818.platform9.io
        > Username : rajnikanth@ekayaa.co.in
        > Password : A56n4aCrAsejBBW!
        > Region [RegionOne]: <Press Enter>
        > Tenant [service]: <Press Enter>
    > pf9ctl prep-node

5. Create cluster from PF9 Web UI
    a. Select 'BareOS Virtual Machine' from options, where to deploy kubernetes cluster.
    b. Click on 'Single Master Cluster'.
    c. Step. 1 - Initial Setup
        - Select Kubernetes Version
        - Under 'Application & Container Settings' section, UNCHECK 'Make Master nodes Master + Worker'
        - Under 'Cluster Add-Ons' section, UNCHECK 'Deploy MetalLB - Layer 2 Mode'
        - Click on Next
    d. Step. 2 - Select Master Node aka Master + Worker ( nodeFor=appServer )
    e. Step. 3 - Select Worker Nodes aka Metallb ( nodeFor=metallb )
    f. Step. 4 - Network Settings, Just Click on Next
    g. Step. 5 - Advanced Configuration, Just Click on Next
    h. Step. 6 - Finish and Review, click on complete.

6. Download & Set kubeconfig on local machine.
    a. Download kubeconfig file from Platform9's Cluster
        - To access your cluster’s kubeconfig, log into your Platform9's Cloud Manager account and navigate to the Cluster section.
        - From the Clusters page, click on the 'kubeconfig' hyper link under 'link' column. The file will be saved to your computer’s Downloads folder.
    b. Copy your downloaded kubeconfig file, <cluster-name>.yaml file to the ~/.kube/configs directory.
        > cp ./kubeconfig/<cluster-name>.yaml ~/.kube/configs/<cluster-name>.yaml
    c. Open up your Bash profile (e.g. ~/.zshenv) in the text editor of your choice and add your configuration file to the $KUBECONFIG PATH variable. If an export KUBECONFIG line is already present in the file, append to the end of this line as follows; if it is not present, add this line to the end of your file.
            > export KUBECONFIG=$KUBECONFIG:~/.kube/config:~/.kube/configs/<cluster-name>.yaml
    d. Close your terminal window and open a new window to receive the changes to the $KUBECONFIG variable.
            > kubectl version
            > kubectl config get-contexts (list all preconfigured contexts and see which one is active:)
            > kubectl config use-context <context-name> (Switch to a context/cluster > #NAME)
            > kubectl get nodes


/****************** Worker Node ********************************************************************************************************************/

1. Setup Worker nodes in Pf9 cluster
    a. Check/Create volume path on k8's Worker node machine.
        > cd /
        > mkdir /tridasha/data/dgraph/
    b. Add labels to worker node
        - Add label 'appServer' for Application specific node.
            > kubectl get nodes --show-labels
            > kubectl label nodes <application-server-node-name> nodeFor=appServer
            > kubectl get nodes --show-labels
        - Add label 'metallb' for Metallb Loadbalancer + Nginx Ingress Controller.
            > kubectl get nodes --show-labels
            > kubectl label nodes <metallb-server-node-name> nodeFor=metallb
            > kubectl get nodes --show-labels


 /****************** Storage ************************************************************************************************************************/

2. Set up storage
    a. Check volume path on k8's node machine.
        > cd /tridasha/data/dgraph/
    b. Create Storage Class on K8's cluster
        > kubectl get sc
        > kubectl create -f ./storage/local-storage.yaml
        > kubectl get sc
    c. Create Persistent Volumes for Dgraph
        - Edit ./storage/persistent-volume-dgraph.yaml
            + add all e2e node name in the list.
        > kubectl get pv
        > kubectl create -f ./storage/persistent-volume-dgraph.yaml
        > kubectl get pv

/****************** Dgraph *************************************************************************************************************************/

3. Setup Dgraph Cluster
    a. Start Dgraph in Single instance mode
        > kubectl create -f ./pod/dgraph-single.yaml
        > kubectl get all -l app=dgraph -o=wide
        > kubectl get pvc
        > kubectl get pv
        > ls -all /data/dgraph/
        > kubectl logs --follow dgraph-0 alpha


/****************** App Server *********************************************************************************************************************/

4. Setup App Server Cluster
    a. Configure docker hub registry with credentials
        > kubectl get secret
        - Run the commands in ./pod/secret.txt
        > kubectl get secret
    b. Start App Server in HA mode.
        > kubectl create -f ./pod/app-server-HA.yaml 
        > kubectl get all -l app=indcatalog
        > kubectl logs --tail=100 -l app=indcatalog
    c. Remove App Server in HA mode.
        > kubectl delete -f ./pod/app-server-HA.yaml 
        > kubectl get pods

/****************** UI Node Server *********************************************************************************************************************/

5. Setup App Server Cluster
    a. Configure docker hub registry with credentials
        > kubectl get secret
        - Run the commands in ./pod/secret.txt
        > kubectl get secret
    b. Start UI Server in HA mode.
        > kubectl create -f ./pod/ui-server-HA.yaml 
        > kubectl get all -l app=ui
        > kubectl logs --tail=100 -l app=ui
    c. Remove UI Server in HA mode.
        > kubectl delete -f ./pod/ui-server-HA.yaml 
        > kubectl get pods

/****************** TLS Certificate ****************************************************************************************************************/

6. Install Certificate for HTTPS
    a. Install cert-manager
        > kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.1/cert-manager.crds.yaml
        > kubectl create namespace cert-manager
        > helm repo add jetstack https://charts.jetstack.io
        > helm repo update
        > helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.15.1
        > kubectl create -f ./certificate/acme-issuer-prod.yaml
    b. Debug Commands
        > kubectl get CertificateRequest -o wide
        > kubectl get certificates -o wide

/****************** LoadBalancer Metallb **********************************************************************************************************/

7. Install Loadbalancer for ServiceType = LoadBalancer
    a. Apply the MetalLB manifest namespace from the namespace.yaml file:
        > kubectl create -f ./loadbalancer-metallb/namespace.yaml
    b. Create the MetalLB secret memberlist:
        > kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)" 
    c. Apply the MetalLB manifest controller and speaker from the metallb.yaml file:
        - Changes done are marked with #Kustomized tag.
            > kubectl create -f ./loadbalancer-metallb/metallb.yaml
    d. UPDATE the 'addresses' field with metallb node IP
        - Apply the MetalLB configmap from the metallb-config.yaml file:
            > kubectl create -f ./loadbalancer-metallb/metallb-config.yaml


/****************** Ingress ************************************************************************************************************************/

    6. Setup Ingress Controller (to handle request redirection based on incoming host/domain name)
        a. Use one of the Metallb node's public IP to add the loadBalancerIP in ./ingress/nginx.values.yaml 
        (search for 'Metallb LoadBalancer IP').
        b. Install nginx-ingress controller on type 'LoadBalancer'
            > helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
            > helm repo update
            > helm install ingress-nginx ingress-nginx/ingress-nginx --version 3.26.0 --values ./ingress/nginx.values.yaml 
            > kubectl get svc -o wide
            - Update the LoadBalancer External IP in GoDaddy.
        b. Configure ingress resources for services to be access over internet
            -  Create ingress resource for Indcatalog service
                > kubectl get ingress -A
                > kubectl create -f ./ingress/nginx-ingress-indcatalog.yaml
                > kubectl get ingress -A
            -  Debug Commands
                > kubectl get CertificateRequest -o wide
                > kubectl get certificates -o wide
        c. Update HELM charts / nginx-ingress controller
            - push nginx config changes to existing nginx-ingress controller 
                > helm upgrade --values ./ingress/nginx.values.yaml ingress-nginx ingress-nginx/ingress-nginx 

===== Dgraph DATA Backup ========================================================================================================================================

1. Connect to 'server' container inside 'dgraph' pod 
    - kubectl exec -it <pod-name> --container <container-name-inside-pod> -- /bin/bash
        > kubectl exec -it dgraph-0 --container alpha -- /bin/bash
2. Then fire curl command inside the container 
    > curl http://localhost:8080/admin/export
3. The above command will create an 'export' folder in '/dgraph' directory in the container.
4. The 'export' folder will have two files generated
    - dgraph-1-2021-03-28-13-01.rdf.gz
    - dgraph-1-2021-03-28-13-01.schema.gz
4. The directory '/dgraph' of container is mapped to the node directory '/tridasha/data/dgraph'. 
5. The same files are now accessible using ssh to the node -> cd /tridasha/data//dgraph
6. To tranfer the data from remote server to local machine
    - scp -r <username>@<machine-IP>:<remote-machine-path> <local-machine-path>
        > scp -r  root@172.105.48.191:/tridasha/data/dgraph/export/ /Download/dgraph_data/

===== Dgraph DATA RESTORE/IMPORT ===============================================================================================================================

1. To tranfer the data from local machine to remote server
    - scp -r <local-machine-path> <username>@<machine-IP>:<remote-machine-path>
        > scp -r  /Upload/dgraph_data/ root@172.105.48.191:/tridasha/data/dgraph/import-data

2. Connect to 'server' container inside 'dgraph' pod 
    - kubectl exec -it <pod-name> --container <container-name-inside-pod> -- /bin/bash
        > kubectl exec -it dgraph-0 --container alpha -- /bin/bash
    - fire the data import command, dgraph live -r <path-to-rdf-gz> -s <path-to-schema-gz>
        > dgraph live -r /tridasha/data/dgraph/import/dgraph-1-2021-03-29-14-37.rdf.gz -s /tridasha/data/dgraph/import/dgraph-1-2021-03-29-14-37.schema.gz 